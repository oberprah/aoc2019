package day02

import day02.exercise01.exercise01
import day02.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay02 {

    @Test
    fun testExercise01() {
        assertEquals(3562672, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(Pair(82, 50), exercise02())
    }
}
