package day05

import computer.Computer
import day05.exercise01.exercise01
import day05.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay05 {

    @Test
    fun testExercise01() {
        assertEquals(7286649, exercise01())
    }

    @Test
    fun testComputerPositionModeEquals() {
        val instructionList = mutableListOf(3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8).map { it.toLong() }.toMutableList()
        assertEquals(0, Computer(instructionList).calculate(mutableListOf(0)))
        assertEquals(1, Computer(instructionList).calculate(mutableListOf(8)))
    }

    @Test
    fun testComputerPositionModeLessThan() {
        val instructionList = mutableListOf(3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8).map { it.toLong() }.toMutableList()
        assertEquals(0, Computer(instructionList).calculate(mutableListOf(9)))
        assertEquals(1, Computer(instructionList).calculate(mutableListOf(7)))
    }

    @Test
    fun testComputerImmediateModeEquals() {
        val instructionList = mutableListOf(3, 3, 1108, -1, 8, 3, 4, 3, 99).map { it.toLong() }.toMutableList()
        assertEquals(0, Computer(instructionList).calculate(mutableListOf(0)))
        assertEquals(1, Computer(instructionList).calculate(mutableListOf(8)))
    }

    @Test
    fun testComputerImmediateModeLessThan() {
        val instructionList = mutableListOf(3, 3, 1107, -1, 8, 3, 4, 3, 99).map { it.toLong() }.toMutableList()
        assertEquals(0, Computer(instructionList).calculate(mutableListOf(9)))
        assertEquals(1, Computer(instructionList).calculate(mutableListOf(7)))
    }

    @Test
    fun testComputerJumpPositionMode() {
        val instructionList = mutableListOf(3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9).map { it.toLong() }.toMutableList()
        assertEquals(0, Computer(instructionList).calculate(mutableListOf(0)))
        assertEquals(1, Computer(instructionList).calculate(mutableListOf(4)))
    }

    @Test
    fun testComputerLargerExample() {
        val instructionList = mutableListOf(3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99).map { it.toLong() }.toMutableList()
        assertEquals(999, Computer(instructionList).calculate(mutableListOf(7)))
        assertEquals(1000, Computer(instructionList).calculate(mutableListOf(8)))
        assertEquals(1001, Computer(instructionList).calculate(mutableListOf(9)))
    }

    @Test
    fun testExercise02() {
        assertEquals(15724522, exercise02())
    }
}
