package day06

import day06.exercise01.exercise01
import day06.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay06 {
    @Test
    fun testExercise01() {
        assertEquals(140608, exercise01())
    }
    @Test
    fun testExercise02() {
        assertEquals(337, exercise02())
    }
}
