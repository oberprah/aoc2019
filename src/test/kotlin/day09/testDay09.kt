package day09

import computer.Computer
import day04.exercise01.toDigitList
import day09.exercise01.calculateOutput
import day09.exercise01.exercise01
import day09.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay09 {
    @Test
    fun testRelativeBase() {
        val instructionList = mutableListOf(109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99).map { it.toLong() }.toMutableList()
        assertEquals(instructionList, calculateOutput(instructionList))
    }

    @Test
    fun testLongOutput() {
        val instructionList = mutableListOf<Long>(1102,34915192,34915192,7,4,7,99,0)
        assertEquals(16, Computer(instructionList).calculate()?.toDigitList()?.size)
    }

    @Test
    fun testLong() {
        val instructionList = mutableListOf<Long>(104,1125899906842624,99)
        assertEquals(1125899906842624, Computer(instructionList).calculate())
    }

    @Test
    fun testExercise01() {
        assertEquals(2465411646, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(69781, exercise02())
    }
}
