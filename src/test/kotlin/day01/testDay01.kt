package day01

import day01.exercise01.exercise01
import day01.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay01 {

    @Test
    fun testExercise01() {
        assertEquals(3427947, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(5139037, exercise02())
    }
}
