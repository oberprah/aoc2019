package day03


import day03.exercise01.exercise01
import day03.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay03 {

    @Test
    fun testExercise01() {
        assertEquals(627, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(13190, exercise02())
    }

}
