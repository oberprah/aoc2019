package day08

import day08.exercise01.exercise01
import day08.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay08 {
    @Test
    fun testExercise01() {
        assertEquals(1560, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(
                "100100110001100100101001010010100101001010010100101001010000100001001011110100101011010000100101001010010100101001010010100100110001110011000110010010",
                exercise02())
    }
}
