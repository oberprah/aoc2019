package day04

import day04.exercise01.exercise01
import day04.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay04 {

    @Test
    fun testExercise01() {
        assertEquals(1079, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(699, exercise02())
    }
}
