package day07

import day07.exercise01.exercise01
import day07.exercise02.exercise02
import org.junit.Test
import kotlin.test.assertEquals

class TestDay07 {

    @Test
    fun testExercise01() {
        assertEquals(262086, exercise01())
    }

    @Test
    fun testExercise02() {
        assertEquals(5371621, exercise02())
    }
}
