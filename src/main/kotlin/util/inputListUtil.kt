package util

import java.io.File


fun getInputAsLongList(path: String): MutableList<Long> {
    return getInputAsList(path) { x -> x.toLong() }
}

fun getInputAsIntList(path: String): MutableList<Int> {
    return getInputAsList(path) { x -> x.toInt() }
}

fun <T> getInputAsList(path: String, convertInput: (String) -> T): MutableList<T> {
    val inputList = mutableListOf<T>()
    File(path).forEachLine { line ->
        if (line.contains(",")) {
            val innerList = mutableListOf<T>()
            line.split(",").forEach {
                innerList += convertInput(it)
            }
            inputList += innerList
        } else {
            inputList += convertInput(line)
        }
    }
    return inputList
}

fun <T> getInputAsNestedList(path: String, convertInput: (String) -> T): MutableList<MutableList<T>> {
    val inputList = mutableListOf<MutableList<T>>()
    File(path).forEachLine { line ->
        if (line.contains(",")) {
            val innerList = mutableListOf<T>()
            line.split(",").forEach {
                innerList += convertInput(it)
            }
            inputList.add(innerList)
        } else {
            inputList.add(mutableListOf(convertInput(line)))
        }
    }
    return inputList
}


