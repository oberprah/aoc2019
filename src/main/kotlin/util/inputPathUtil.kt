package util

fun getInputPath(day: String, exercise: String? = null): String {
    return "src/main/kotlin/day$day/${exercise?.let { "exercise$exercise" } ?: run {""}}/input.txt"
}
