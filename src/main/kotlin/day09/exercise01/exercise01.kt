package day09.exercise01

import computer.Computer
import util.getInputAsLongList
import util.getInputPath

fun exercise01(): Long {
    val instructionList = getInputAsLongList(getInputPath("09"))
    return Computer(instructionList).calculate(mutableListOf(1)) as Long
}

fun calculateOutput(instructionList: MutableList<Long>): MutableList<Long> {
    val outputList = mutableListOf<Long>()
    val computer = Computer(instructionList)
    while (!computer.isStopped())
        computer.calculate()?.let { outputList.add(it) }
    return outputList
}

fun main() {
    println(exercise01())
}
