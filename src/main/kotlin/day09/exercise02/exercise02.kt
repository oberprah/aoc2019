package day09.exercise02

import computer.Computer
import util.getInputAsLongList
import util.getInputPath

fun exercise02(): Long {
    val instructionList = getInputAsLongList(getInputPath("09"))
    return Computer(instructionList).calculate(mutableListOf(2)) as Long
}

fun main() {
    println(exercise02())
}
