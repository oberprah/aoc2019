package day02.exercise02

import day02.exercise01.opCodeCalculator
import util.getInputAsIntList
import util.getInputPath
import java.util.*

fun exercise02(): Pair<Int, Int> {
    val inputList = getInputAsIntList(getInputPath("02"))
    return searchVerbAndNone(inputList, 19690720)
}

fun searchVerbAndNone(list: MutableList<Int>, expectedOutput: Int): Pair<Int, Int> {
    for (noun in 0..99) {
        for (verb in 0..99) {
            try {
                val copyOfList = list.toMutableList()
                opCodeCalculator(copyOfList, noun, verb)
                if (copyOfList[0] == expectedOutput)
                    return Pair(noun, verb)
            } catch (e: IndexOutOfBoundsException) {
            }
        }
    }
    throw NoSuchElementException()
}

fun main() {
    println(exercise02())
}