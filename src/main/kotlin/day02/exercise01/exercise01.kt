package day02.exercise01

import util.getInputAsIntList
import util.getInputPath

const val EXIT = 99
const val ADD = 1
const val MUL = 2

fun exercise01(): Int {
    val inputList = getInputAsIntList(getInputPath("02"))
    opCodeCalculator(inputList, 12, 2)
    return inputList[0]
}

fun opCodeCalculator(list: MutableList<Int>, noun: Int, verb: Int) {
    list[1] = noun
    list[2] = verb

    var index = 0
    while (list[index] != EXIT) {
        when (list[index]) {
            ADD -> binaryOperation(list, index) { x, y -> x + y}
            MUL -> binaryOperation(list, index) { x, y -> x * y}
        }
        index += 4
    }
}

fun binaryOperation(list: MutableList<Int>, index: Int, operation: (Int, Int) -> Int) {
    val indexFirst = list[index+1]
    val indexSecond = list[index+2]
    val indexResult = list[index+3]
    list[indexResult] = operation(list[indexFirst], list[indexSecond])
}

fun main() {
    println(exercise01())
}
