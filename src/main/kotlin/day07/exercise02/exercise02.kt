package day07.exercise02

import computer.Computer
import day07.exercise01.createPermutation
import util.getInputAsLongList
import util.getInputPath

fun exercise02(): Long {
    return createPermutation(listOf(5, 6, 7, 8, 9))
            .map { calculateThrusterSignal(it.toMutableList()) }
            .max() as Long
}

fun calculateThrusterSignal(amplifierSequence: MutableList<Int>): Long {
    val computerList = List(amplifierSequence.size) { Computer(getInputAsLongList(getInputPath("07"))) }
    val amplifierResult = MutableList(amplifierSequence.size + 1) { 0.toLong() }

    while (!computerList.all { it.isStopped() }) {
        for (i in computerList.indices) {
            val instructionList = if (amplifierSequence.isNotEmpty())
                mutableListOf(amplifierSequence.removeAt(0).toLong(), amplifierResult[i])
            else mutableListOf(amplifierResult[i])
            computerList[i].calculate(instructionList)?.let { amplifierResult[i + 1] = it }
            if (i == computerList.lastIndex)
                amplifierResult[0] = amplifierResult[i + 1]
        }
    }
    return amplifierResult.last()
}

fun main() {
    print(exercise02())
}
