package day07.exercise01

import computer.Computer
import util.getInputAsLongList
import util.getInputPath


fun exercise01(): Int {
    return createPermutation(listOf(0, 1, 2, 3, 4))
            .map { calculateThrusterSignal(it) }
            .max() as Int
}

fun calculateThrusterSignal(amplifierSequence: List<Int>): Int {
    val amplifierResult = IntArray(amplifierSequence.size + 1) { 0 }
    for (i in amplifierSequence.indices) {
        val instructions = getInputAsLongList(getInputPath("07"))
        amplifierResult[i + 1] = Computer(instructions).calculate(mutableListOf(amplifierSequence[i].toLong(), amplifierResult[i].toLong()))!!.toInt()
    }
    return amplifierResult.last()
}

fun createPermutation(digits: List<Int>, prefix: List<Int> = emptyList()): List<List<Int>> {
    return if (digits.isEmpty())
        listOf(prefix)
    else {
        val permutations = mutableListOf<List<Int>>()
        (digits.indices).forEach {
            permutations += createPermutation(digits.subList(0, it) + digits.subList(it + 1, digits.size), prefix + digits[it])
        }
        permutations
    }
}

fun main() {
    println(exercise01())
}
