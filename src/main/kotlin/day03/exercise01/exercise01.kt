package day03.exercise01

import util.getInputAsNestedList
import util.getInputPath
import kotlin.math.absoluteValue

data class Step(val direction: Char, val number: Int)

data class Location(val horizontal: Int, val vertical: Int)


fun exercise01(): Int {
    val inputList = getInputAsNestedList(getInputPath("03")) { x -> Step(x[0], x.substring(1).toInt()) }

    val manhattanDistances = getIntersections(inputList[0], inputList[1])
            .map { it.vertical.absoluteValue + it.horizontal.absoluteValue }

    return manhattanDistances.min() as Int
}

fun getIntersections(firstList: List<Step>, secondList: List<Step>): Set<Location> {
    val visitedPointsFirstWire = mutableSetOf<Location>().addWire(firstList)
    val visitedPointsSecondWire = mutableSetOf<Location>().addWire(secondList)

    return visitedPointsFirstWire.intersect(visitedPointsSecondWire)
}

fun MutableSet<Location>.addWire(wires: List<Step>): MutableSet<Location> {
    var horizontal = 0
    var vertical = 0

    wires.forEach {
        when (it.direction) {
            'R' -> for (i in 1..it.number) {
                horizontal++
                this.add(Location(horizontal, vertical))
            }
            'L' -> for (i in 1..it.number) {
                horizontal--
                this.add(Location(horizontal, vertical))
            }
            'U' -> for (i in 1..it.number) {
                vertical++
                this.add(Location(horizontal, vertical))
            }
            'D' -> for (i in 1..it.number) {
                vertical--
                this.add(Location(horizontal, vertical))
            }
        }
    }
    return this
}

fun main() {
    println(exercise01())
}