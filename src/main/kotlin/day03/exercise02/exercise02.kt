package day03.exercise02

import day03.exercise01.Location
import day03.exercise01.Step
import day03.exercise01.getIntersections
import util.getInputAsNestedList
import util.getInputPath

fun exercise02(): Int {
    val inputList = getInputAsNestedList(getInputPath("03")) { x -> Step(x[0], x.substring(1).toInt()) }

    val intersections = getIntersections(inputList[0], inputList[1])

    return intersections
            .map { inputList[0].stepsToReachIntersection(it) + inputList[1].stepsToReachIntersection(it) }
            .min() as Int
}

fun List<Step>.stepsToReachIntersection(intersection: Location): Int {
    var steps = 0

    var horizontal = 0
    var vertical = 0

    this.forEach {
        when (it.direction) {
            'R' -> for (i in 1..it.number) {
                steps++
                horizontal++
                if (intersection == Location(horizontal, vertical))
                    return steps
            }
            'L' -> for (i in 1..it.number) {
                steps++
                horizontal--
                if (intersection == Location(horizontal, vertical))
                    return steps
            }
            'U' -> for (i in 1..it.number) {
                steps++
                vertical++
                if (intersection == Location(horizontal, vertical))
                    return steps
            }
            'D' -> for (i in 1..it.number) {
                steps++
                vertical--
                if (intersection == Location(horizontal, vertical))
                    return steps
            }
        }
    }
    throw NoSuchElementException()
}

fun main() {
    print(exercise02())
}
