package computer

class InstructionList(
        private val instructionList: MutableMap<Int, Long> = mutableMapOf()
) {
    operator fun get(index: Int): Long = instructionList[index] ?: 0

    operator fun set(index: Int, element: Long) {
        instructionList[index] = element
    }

    override fun toString(): String = instructionList.toString()
}

fun instructionListOf(list: List<Long>): InstructionList {
    val instructionList = InstructionList()
    for (i in list.indices)
        instructionList[i] = list[i]
    return instructionList
}
