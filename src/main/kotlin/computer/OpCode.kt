package computer

enum class OpCode(val value: Long) {
    EXIT(99) {
        override fun execute(computerState: ComputerState) {
            computerState.calculationState = CalculationState.STOPPED
        }
    },
    ADD(1) {
        override fun execute(computerState: ComputerState) {
            computerState.binaryCalculation { x, y -> x + y }
        }
    },
    MUL(2) {
        override fun execute(computerState: ComputerState) {
            computerState.binaryCalculation { x, y -> x * y }
        }
    },
    INPUT(3) {
        override fun execute(computerState: ComputerState) {
            val indexParameter = computerState.getIndexForParameter(0)
            computerState.instructionList[indexParameter] = computerState.inputList.removeAt(0)
            computerState.index += 2
        }
    },
    OUTPUT(4) {
        override fun execute(computerState: ComputerState) {
            computerState.output = computerState.getValueForParameter(0)
            computerState.index += 2
            if (computerState.pausedOnOutput)
                computerState.calculationState = CalculationState.PAUSED
        }
    },
    JUMP_IF_TRUE(5) {
        override fun execute(computerState: ComputerState) {
            if (computerState.getValueForParameter(0) != 0.toLong())
                computerState.index = computerState.getValueForParameter(1).toInt()
            else
                computerState.index += 3
        }
    },
    JUMP_IF_FALSE(6) {
        override fun execute(computerState: ComputerState) {
            if (computerState.getValueForParameter(0) == 0.toLong())
                computerState.index = computerState.getValueForParameter(1).toInt()
            else
                computerState.index += 3
        }
    },
    LESS_THAN(7) {
        override fun execute(computerState: ComputerState) {
            computerState.binaryCalculation { x, y -> if (x < y) 1 else 0 }
        }
    },
    EQUAL(8) {
        override fun execute(computerState: ComputerState) {
            computerState.binaryCalculation { x, y -> if (x == y) 1 else 0 }
        }
    },
    ADJUST_INDEX(9) {
        override fun execute(computerState: ComputerState) {
            val relativeIndexMove = computerState.getValueForParameter(0).toInt()
            computerState.relativeBase += relativeIndexMove
            computerState.index += 2
        }
    };

    abstract fun execute(computerState: ComputerState)

    companion object {
        private val map = values().associateBy(OpCode::value)
        fun fromLong(type: Long) = map[type]
    }
}
