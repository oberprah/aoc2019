package computer

class Computer(
        instructionList: MutableList<Long>,
        pausedOnOutput: Boolean = true
) {
    private val state = ComputerState(instructionListOf(instructionList), pausedOnOutput = pausedOnOutput)

    fun calculate(inputList: MutableList<Long> = mutableListOf()): Long? {
        state.inputList.addAll(inputList)
        state.output = null
        executeInstructionList()
        return state.output
    }

    private fun executeInstructionList() {
        state.calculationState = CalculationState.RUNNING
        while (state.calculationState == CalculationState.RUNNING) {
            state.getCurrentOpCode().execute(state)
        }
    }

    fun isStopped() = state.calculationState == CalculationState.STOPPED
}