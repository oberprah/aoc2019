package computer

enum class ParameterMode(val value: Int) {
    POSITION(0) {
        override fun getIndex(computerState: ComputerState, positionParameter: Int): Int {
            val indexParameter = computerState.index + positionParameter + 1
            return computerState.instructionList[indexParameter].toInt()
        }
    },
    IMMEDIATE(1) {
        override fun getIndex(computerState: ComputerState, positionParameter: Int): Int {
            return computerState.index + positionParameter + 1
        }
    },
    RELATIVE(2) {
        override fun getIndex(computerState: ComputerState, positionParameter: Int): Int {
            val indexParameter = computerState.index + positionParameter + 1
            return (computerState.relativeBase + computerState.instructionList[indexParameter]).toInt()
        }
    };

    abstract fun getIndex(computerState: ComputerState, positionParameter: Int): Int

    companion object {
        private val map = values().associateBy(ParameterMode::value)
        fun fromInt(type: Int) = map[type]
    }
}
