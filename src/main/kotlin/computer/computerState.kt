package computer

import day04.exercise01.toDigitList

enum class CalculationState {
    RUNNING,
    PAUSED,
    STOPPED,
}

data class ComputerState(
        val instructionList: InstructionList,
        var pausedOnOutput: Boolean,
        var index: Int = 0,
        var relativeBase: Int = 0,
        var inputList: MutableList<Long> = mutableListOf(),
        var output: Long? = null,
        var calculationState: CalculationState = CalculationState.PAUSED)

fun ComputerState.binaryCalculation(calculation: (Long, Long) -> Long) {
    val indexFirstParameter = this.getIndexForParameter(0)
    val indexSecondParameter = this.getIndexForParameter(1)
    val indexResult = this.getIndexForParameter(2)
    this.instructionList[indexResult] = calculation(this.instructionList[indexFirstParameter], this.instructionList[indexSecondParameter])
    this.index += 4
}

fun ComputerState.getCurrentOpCode(): OpCode {
    val currentInstruction = this.getCurrentInstruction()
    return OpCode.fromLong(currentInstruction % 100)
            ?: throw IllegalArgumentException("OpCode ${currentInstruction % 100} not supported")
}

fun ComputerState.getValueForParameter(positionParameter: Int): Long = this.instructionList[this.getIndexForParameter(positionParameter)]

fun ComputerState.getIndexForParameter(positionParameter: Int): Int {
    val position = positionParameter + 2
    val digits = this.getCurrentInstruction().toDigitList().reversed()

    if (position > digits.lastIndex)
        return ParameterMode.POSITION.getIndex(this, positionParameter)

    return (ParameterMode.fromInt(digits[position].toInt()) as ParameterMode)
            .getIndex(this, positionParameter)
}

fun ComputerState.getCurrentInstruction(): Long = this.instructionList[this.index]
