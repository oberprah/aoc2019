package day08.exercise02

import day08.exercise01.LAYER_HEIGHT
import day08.exercise01.LAYER_WIDTH
import day08.exercise01.toLayers
import util.getInputAsList
import util.getInputPath

const val BLACK = 0
const val WHITE = 1
const val TRANSPARENT = 2

fun exercise02(): String {
    val inputList = getInputAsList(getInputPath("08")) { x -> x.toCharArray().map { it.toInt() - 48 } }[0].toMutableList()
    val layers = inputList.toLayers()
    val result = MutableList(layers.first().size) { TRANSPARENT }

    layers.forEach {
        for (i in result.indices)
            if (result[i] == TRANSPARENT)
                result[i] = it[i]
    }

    return result.joinToString("")
}

fun main() {
    println(exercise02())
}
