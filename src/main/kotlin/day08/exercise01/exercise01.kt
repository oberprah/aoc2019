package day08.exercise01

import util.getInputAsList
import util.getInputPath


const val LAYER_WIDTH = 25
const val LAYER_HEIGHT = 6

fun exercise01(): Int {
    val inputList = getInputAsList(getInputPath("08")) { x -> x.toCharArray().map { it.toInt() - 48 } }[0].toMutableList()
    val layers = inputList.toLayers()

    val layerMinZeros = layers.minBy { layer -> layer.count { it == 0 } } as List<Int>
    return layerMinZeros.count { it == 1 } * layerMinZeros.count { it == 2 }
}

fun MutableList<Int>.toLayers(): MutableList<MutableList<Int>> {
    val layers = mutableListOf<MutableList<Int>>()
    while (this.isNotEmpty()) {
        layers.add(mutableListOf())
        for (i in 0 until LAYER_WIDTH * LAYER_HEIGHT)
            layers.last().add(this.removeAt(0))
    }
    return layers
}

fun main() {
    println(exercise01())
}
