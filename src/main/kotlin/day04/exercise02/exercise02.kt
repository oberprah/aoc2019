package day04.exercise02

import day04.exercise01.digitsAreNotDecreasing
import day04.exercise01.toDigitList

fun exercise02(): Int {
    var count = 0
    for (i in 245318..765747.toLong())
        if (i.containsExactlyTwoTimesTheSameDigit() && i.digitsAreNotDecreasing())
            count++
    return count
}

fun Long.containsExactlyTwoTimesTheSameDigit(): Boolean {
    val digits = this.toDigitList()
    for (i in 0..9.toLong())
        if (digits.filter { it == i }.size == 2)
            return true
    return false
}

fun main() {
    println(exercise02())
}
