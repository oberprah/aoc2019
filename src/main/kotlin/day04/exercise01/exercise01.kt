package day04.exercise01

fun exercise01(): Int {
    var count = 0
    for (i in 245318..765747.toLong())
        if (i.containsTwoAdjustingSameNumbers() && i.digitsAreNotDecreasing())
            count++
    return count
}

fun Long.containsTwoAdjustingSameNumbers(): Boolean {
    val digits = this.toDigitList()
    for (i in 0..digits.size - 2)
        if (digits[i] == digits[i + 1])
            return true
    return false
}

fun Long.digitsAreNotDecreasing(): Boolean {
    val digits = this.toDigitList()
    for (i in 0..digits.size - 2)
        if (digits[i] > digits[i + 1])
            return false
    return true
}

fun Long.toDigitList(): List<Long> {
    val digits = mutableListOf<Long>()

    var number = this
    while (number != 0.toLong()) {
        digits.add(number % 10)
        number /= 10
    }

    return digits.reversed()
}

fun main() {
    println(exercise01())
}
