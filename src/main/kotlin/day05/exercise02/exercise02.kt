package day05.exercise02

import computer.Computer
import util.getInputAsLongList
import util.getInputPath

fun exercise02(): Long {
    val list = getInputAsLongList(getInputPath("05"))
    return Computer(list).calculate(mutableListOf(5)) as Long
}

fun main() {
    println(exercise02())
}