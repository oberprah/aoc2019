package day05.exercise01

import computer.Computer
import util.getInputAsLongList
import util.getInputPath


fun exercise01(): Long {
    val list = getInputAsLongList(getInputPath("05"))
    return Computer(list, pausedOnOutput = false).calculate(mutableListOf(1)) as Long
}

fun main() {
    println(exercise01())
}
