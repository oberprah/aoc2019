package day06.exercise02

import day06.exercise01.OrbitMap
import util.getInputAsList
import util.getInputPath

fun exercise02(): Int {
    val orbitMapList = getInputAsList(getInputPath("06")) { x -> OrbitMap(x.split(")")[0], x.split(")")[1]) }

    val youOrbitsWithSteps = "YOU".getOrbitsWithSteps(orbitMapList)
    val sanOrbitsWithSteps = "SAN".getOrbitsWithSteps(orbitMapList)

    return youOrbitsWithSteps
            .filter { sanOrbitsWithSteps.keys.contains(it.key) }
            .map { it.value + sanOrbitsWithSteps[it.key] as Int }
            .min() as Int - 2
}

fun String.getOrbitsWithSteps(orbitMapList: List<OrbitMap>): Map<String, Int> {
    val orbitsWithSteps = mutableMapOf<String, Int>()

    orbitMapList
            .filter { it.orbit == this }
            .forEach { orbitMap ->
                orbitsWithSteps +=
                        mutableMapOf(Pair(orbitMap.center, 1)) +
                                orbitMap.center.getOrbitsWithSteps(orbitMapList).map { Pair(it.key, it.value + 1) }
            }

    return orbitsWithSteps
}

fun main() {
    println(exercise02())
}
