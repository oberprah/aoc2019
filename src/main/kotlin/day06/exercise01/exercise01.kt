package day06.exercise01

import util.getInputAsList
import util.getInputPath

data class OrbitMap(
        val center: String,
        val orbit: String)

fun exercise01(): Int {
    val orbitMapList = getInputAsList(getInputPath("06")) { x -> OrbitMap(x.split(")")[0], x.split(")")[1]) }

    return orbitMapList
            .map { it.orbit }
            .toSet()
            .map { it.getNumberOrbits(orbitMapList) }
            .sum()
}

fun String.getNumberOrbits(orbitMapList: List<OrbitMap>): Int {
    return if (orbitMapList.map { it.orbit }.contains(this))
        1 + orbitMapList
                .filter { it.orbit == this }
                .map { it.center.getNumberOrbits(orbitMapList) }
                .sum()
    else
        0
}

fun main() {
    println(exercise01())
}
