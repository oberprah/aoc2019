package day01.exercise02

import day01.exercise01.getFuelForMass
import util.getInputAsIntList
import util.getInputPath

fun exercise02(): Int {
    val inputList = getInputAsIntList(getInputPath("01"))

    var requiredFuel = 0;
    inputList.map { requiredFuel += getFuelForMassRecursive(it) }

    return requiredFuel
}

fun getFuelForMassRecursive(mass: Int): Int {
    val fuel = getFuelForMass(mass)
    return if (fuel <= 0) {
        0
    } else {
        fuel + getFuelForMassRecursive(fuel)
    }
}

fun main() {
    val requiredFuel = exercise02()
    println("Required Fuel: $requiredFuel")
}
