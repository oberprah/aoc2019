package day01.exercise01

import util.getInputAsIntList
import util.getInputPath


fun exercise01(): Int {
    val inputList = getInputAsIntList(getInputPath("01"))

    var requiredFuel = 0
    inputList.map { requiredFuel += getFuelForMass(it) }

    return requiredFuel
}

fun getFuelForMass(mass: Int): Int {
    return ((mass / 3.0).toInt()) - 2
}

fun main() {
    val requiredFuel = exercise01()
    println("Required Fuel: $requiredFuel")
}
